package com.iqbal.ambang.pelatihan.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Peserta {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(nullable = false)
    @NotNull()
    @NotEmpty()
    @Size(min = 3, max = 150)
    private String nama;

    @Column(nullable = false, unique = true)
    @Email()
    @NotNull()
    @NotEmpty()
    private String email;

    @Column(name = "tanggal_lahir", nullable = false)
    @Past()
    @NotNull()
    @Temporal(TemporalType.DATE)
    private Date tanggalLahir;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
}
