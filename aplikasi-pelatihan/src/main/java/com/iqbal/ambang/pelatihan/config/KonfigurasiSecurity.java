package com.iqbal.ambang.pelatihan.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import javax.sql.DataSource;


@EnableWebSecurity
@Configuration
public class KonfigurasiSecurity extends WebSecurityConfigurerAdapter {


    private static final String SQL_LOGIN = "select username, password, active as enabled "
            + " from s_users where username = ?";


    private static final String SQL_PERMISSION = "select u.username, r.nama as authorization "
            + " from s_users u join s_user_role ur on u.id = ur.id_user "
            + " join s_roles r on ur.id_role = r.id "
            + " where u.username = ?";


    @Autowired
    private DataSource ds;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth
                .jdbcAuthentication()
                .dataSource(ds)
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .usersByUsernameQuery(SQL_LOGIN)
                .authoritiesByUsernameQuery(SQL_PERMISSION);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/halo").hasAnyRole("ADMIN", "STAFF")
                .antMatchers("/peserta/form").hasRole("ADMIN")
                .antMatchers("/peserta/list").hasRole("STAFF")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/halo")
                .and()
                .logout()
                .permitAll()
                .and()
                .addFilterAfter(new CsrfAttributeToCookieFilter(), CsrfFilter.class)
                .csrf().csrfTokenRepository(csrfTokenRepository());

    }


    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }
}






















    /*@Bean
    @Override
    public UserDetailsService userDetailsService() {

        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        final User.UserBuilder userBuilder = User.builder().passwordEncoder(encoder::encode);
        UserDetails user = userBuilder
                .username("user")
                .password("password")
                .roles("USER")
                .build();

        UserDetails admin = userBuilder
                .username("admin")
                .password("password")
                .roles("USER", "ADMIN")
                .build();

        return new InMemoryUserDetailsManager(user, admin);
    }
}*/


    /*@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth
                .inMemoryAuthentication()
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .withUser("iqbal").password("12345").roles("USER");
    }*/


    /*protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

                auth.jdbcAuthentication().dataSource(ds)
                        /*.passwordEncoder(new BCryptPasswordEncoder())
                        .passwordEncoder(passwordEncoder())
                        .usersByUsernameQuery("select username,password,active as enabled from users where username = ?")
                        .authoritiesByUsernameQuery(" select u.username, r.name from s_users u join s_user_role ur on u.id = ur.id_user " +
                                " join s_roles r on ur.id_role = r.id where u.username =?");
            }

            @Override
            protected void configure(HttpSecurity http) throws Exception {

                http.authorizeRequests()
                        .antMatchers("/public").permitAll()
                        .anyRequest().authenticated()
                        .and().formLogin().permitAll()
                        .and().logout() .permitAll();
            }


            @Bean
            public PasswordEncoder passwordEncoder(){
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                return passwordEncoder;
            }


        }*/