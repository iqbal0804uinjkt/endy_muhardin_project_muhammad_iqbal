package com.iqbal.ambang.pelatihan.dao;

import com.iqbal.ambang.pelatihan.entity.Peserta;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface PesertaDao extends PagingAndSortingRepository<Peserta, String>{

}

