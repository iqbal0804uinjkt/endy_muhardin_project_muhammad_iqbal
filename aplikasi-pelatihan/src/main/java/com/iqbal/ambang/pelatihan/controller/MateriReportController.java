package com.iqbal.ambang.pelatihan.controller;


import com.iqbal.ambang.pelatihan.dao.MateriDao;
import com.iqbal.ambang.pelatihan.entity.Materi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
public class MateriReportController {

    @Autowired
    private MateriDao md;


    @RequestMapping("/materi.pdf")
    public ModelAndView generateReportMateri(ModelAndView m, @RequestParam(value = "format", required = false) String format) {
        Iterable<Materi> data = md.findAll();
        m.addObject("dataSource", data);
        m.addObject("tanggalUpdate", new Date());
        m.addObject("format", "pdf");
        if (format != null && !format.isEmpty()) {
            m.addObject("format", format);
        }
        m.setViewName("report_materi");
        return m;


    }

}
