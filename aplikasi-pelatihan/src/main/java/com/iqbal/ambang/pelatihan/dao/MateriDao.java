package com.iqbal.ambang.pelatihan.dao;

import com.iqbal.ambang.pelatihan.entity.Materi;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface MateriDao extends PagingAndSortingRepository<Materi, String> {
}
