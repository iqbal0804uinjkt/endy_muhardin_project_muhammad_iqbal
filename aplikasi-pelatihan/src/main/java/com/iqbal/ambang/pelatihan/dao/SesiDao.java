package com.iqbal.ambang.pelatihan.dao;

import com.iqbal.ambang.pelatihan.entity.Materi;
import com.iqbal.ambang.pelatihan.entity.Sesi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface SesiDao extends PagingAndSortingRepository<Sesi, String> {
    public Page<Sesi> findByMateri(Materi m, Pageable page);

    @Query("SELECT x FROM Sesi x WHERE x.mulai >= :m "
            + "AND x.mulai < :s "
            + "AND x.materi.kode = :k "
            + "ORDER BY x.mulai DESC ")
    Page<Sesi> cariBerdasarkanTanggalMulaidanKodeMateri(
            @Param("m") Date mulai,
            @Param("s") Date sampai,
            @Param("k") String kode,
            Pageable page
    );

}


