package com.iqbal.ambang.pelatihan.controller;


import com.iqbal.ambang.pelatihan.dao.MateriDao;
import com.iqbal.ambang.pelatihan.entity.Materi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class MateriController {

    @Autowired
    private MateriDao md;


    @RequestMapping(value = "/materi", method = RequestMethod.GET)
    public Page<Materi> daftarMateri(Pageable page) {
        return md.findAll(page);
    }

    @RequestMapping(value = "/materi/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void hapusMateri(@PathVariable("id") String id) {
        md.deleteById(id);
    }

    @RequestMapping(value = "/materi", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void insertMateriBaru(@RequestBody @Valid Materi m) {
        md.save(m);

    }
}
