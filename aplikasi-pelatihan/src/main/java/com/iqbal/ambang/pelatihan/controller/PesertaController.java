package com.iqbal.ambang.pelatihan.controller;

import com.iqbal.ambang.pelatihan.dao.PesertaDao;
import com.iqbal.ambang.pelatihan.entity.Peserta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class PesertaController {

    @Autowired
    private PesertaDao pd;


    @RequestMapping(value = "/peserta", method = RequestMethod.GET)
    public Page<Peserta> cariPeserta(Pageable page) {
        return pd.findAll(page);

    }

    @RequestMapping(value = "/peserta", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void insertPesertaBaru(@RequestBody @Valid Peserta p) {
        pd.save(p);

    }

    @RequestMapping(value = "/peserta/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void updatePeserta(@PathVariable("id") String id, @RequestBody @Valid Peserta p) {
        p.setId(id);
        pd.save(p);
    }

    @GetMapping(path = "/peserta/{id}")
    public ResponseEntity<Peserta> cariPesertaById(@PathVariable @Valid String id) {
        Optional<Peserta> pesertaOpt = pd.findById(id);
        return pesertaOpt.map(peserta -> new ResponseEntity<>(peserta, HttpStatus.OK)).
                orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/peserta/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void hapusPeserta(@PathVariable("id") String id) {
        pd.deleteById(id);
    }


}

    /*@RequestMapping(value = "/peserta/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Peserta> cariPesertaById(@PathVariable("id") String id) {
        Optional<Peserta> hasil = pd.findById(id);
        if (hasil == null) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);}
    }

   /* @RequestMapping(value = "/peserta/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Optional<Peserta> cariPesertaById(@PathVariable("id") String id) {
        return pd.findById(id);

    }*/


